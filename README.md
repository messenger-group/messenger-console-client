# messenger-console-client

# Как запустить

## Аргументы командной строки
- args[0] - chat service host
- args[1] - user service host

## Пример запуска
- dotnet ChatHubClient.dll http://localhost:8082 http://localhost:8081

# Как использовать
- При запуске приложения следуйте инструкциям на консоли