﻿using ChatHubClient;
using ChatHubClient.Services;
using ChatHubClient.Messages;
using Microsoft.AspNetCore.SignalR.Client;

var chatHost = args[0];
var usersHost = args[1];

using var chatClient = new ChatClient(new HttpClient { BaseAddress = new Uri(chatHost) });
using var usersClient = new UsersClient(new HttpClient { BaseAddress = new Uri(usersHost) });

Console.WriteLine("Enter user id or new user information");
Console.WriteLine("User id format: guid");
Console.WriteLine("User information format: name;surname;patronymic(or empty);email");

var userData = Console.ReadLine()!;
if (Guid.TryParse(userData, out var userId))
{
    usersClient.Login(userId);
    Console.WriteLine($"You logged in as {usersClient.CurrentUserId}");
}
else
{
    var newUserInfo = userData.Split(';');
    if (newUserInfo.Length != 4)
    {
        throw new Exception("Invalid User information format. Should be: name;surname;patronymic(or empty);email");
    }

    await usersClient.CreateNewUserAndLogin(
        newUserInfo[0],
        newUserInfo[1],
        string.IsNullOrWhiteSpace(newUserInfo[2]) ? null : newUserInfo[2],
        newUserInfo[3]);
    
    Console.WriteLine($"You logged in as {usersClient.CurrentUserId}");
}

var currenUserId = usersClient.CurrentUserId;
await using var connection = new HubConnectionBuilder()
    .WithUrl(new Uri($"{chatHost}/hub/chat"), options =>
    {
        options.Headers.Add("User-Id", currenUserId.ToString());
    })
    .WithAutomaticReconnect()
    .Build();

var hubClient = new HubClient(new ConsoleWriter(), currenUserId);

connection.On<MessageCreatedData>(HubMessages.MessageSent, hubClient.OnMessageCreated);
connection.On<MessageEditedData>(HubMessages.MessageEdited, hubClient.OnMessageEdited);
connection.On<MessageDeletedData>(HubMessages.MessageDeleted, hubClient.OnMessageDeleted);

await connection.StartAsync();

List<string> commands =
[
    Commands.GetUsers,
    Commands.CreateChat,
    Commands.ChatsList,
    Commands.RestoreChatsList,
    Commands.RestoreMessages,
    Commands.ViewMessages,
    Commands.CreateMessage,
    Commands.EditMessage,
    Commands.DeleteMessage
];

while (true)
{
    try
    {
        WriteCommands();
            
        var command = Console.ReadLine()!;
        if (command == "exit")
        {
            break;
        }
        
        var commandCode = command[..2];
        if (commands.Contains(commandCode) is false)
        {
            throw new Exception("Invalid command code");
        }

        await (commandCode switch
        {
            Commands.GetUsers => HandleGetUsers(usersClient),
            Commands.CreateChat => HandleCreateChat(command, chatClient, hubClient),
            Commands.ChatsList => HandleChatsList(hubClient),
            Commands.RestoreChatsList => HandleRestoreChatsList(hubClient, connection),
            Commands.RestoreMessages => RestoreMessages(command, hubClient, connection),
            Commands.ViewMessages => HandleViewMessages(command, hubClient, connection),
            Commands.CreateMessage => HandleCreateMessage(command, connection),
            Commands.EditMessage => EditMessage(command, connection),
            Commands.DeleteMessage => DeleteMessage(command, connection),
            _ => throw new ArgumentException(commandCode)
        });
            
        Console.WriteLine("Press enter to return to commands");
        Console.ReadLine();
    }
    catch (Exception exception)
    {
        Console.WriteLine(exception.Message);
    }
}

await connection.StopAsync();

return;

static async Task HandleGetUsers(UsersClient usersClient)
{
    foreach (var user in await usersClient.GetUsers())
    {
        Console.WriteLine(user);
    }
}

static async Task HandleCreateChat(string command, ChatClient chatClient, HubClient hubClient)
{
    var userIds = command[3..]
        .Split(';')
        .Select(Guid.Parse)
        .ToList();

    var chatId = await chatClient.CreateChat(userIds);

    hubClient.RestoreChats([chatId]);
    Console.WriteLine($"Chat created. Id: {chatId}");
}

static Task HandleChatsList(HubClient hubClient)
{
    foreach (var chatId in hubClient.GetChats())
    {
        Console.WriteLine(chatId);
    }
    
    return Task.CompletedTask;
}

static async Task HandleRestoreChatsList(HubClient hubClient, HubConnection hubConnection)
{
    var chats = await hubConnection.InvokeAsync<List<ChatDto>>("GetChats");
    hubClient.RestoreChats(chats.Select(x => x.Id));
}

static async Task RestoreMessages(string command, HubClient hubClient, HubConnection hubConnection)
{
    var chatId = Guid.Parse(command[3..]);

    var messages = await hubConnection.InvokeAsync<List<MessageDto>>("GetChatMessages", chatId);
    hubClient.RestoreMessages(chatId, messages.Select(x => new ChatMessage
    {
        Id = x.Id,
        Text = x.Text,
        Sender = x.Sender.FullName
    }));
}

static async Task HandleViewMessages(string command, HubClient hubClient, HubConnection hubConnection)
{
    var chatId = Guid.Parse(command[3..]);

    foreach (var chatMessage in hubClient.GetMessages(chatId))
    {
        Console.WriteLine(chatMessage);
    }

    await hubConnection.InvokeAsync("ReadMessages", new { ChatId = chatId });
}

static async Task HandleCreateMessage(string command, HubConnection hubConnection)
{
    var chatId = Guid.Parse(command.AsSpan(3, 36));
    var messageText = command[40..];
    await hubConnection.InvokeAsync("SendMessage", new CreateMessageData(chatId, messageText));
}

static async Task EditMessage(string command, HubConnection hubConnection)
{
    var messageId = Guid.Parse(command.AsSpan(3, 36));
    var newMessageText = command[40..];
    await hubConnection.InvokeAsync("EditMessage", new EditMessageData(messageId, newMessageText));
}

static async Task DeleteMessage(string command, HubConnection hubConnection)
{
    var messageId = Guid.Parse(command.AsSpan(3, 36));
    await hubConnection.InvokeAsync("DeleteMessage", new DeleteMessageData(messageId));
}

static void WriteCommands()
{
    Console.Clear();
    Console.ForegroundColor = ConsoleColor.Red;
    Console.WriteLine("Commands:");
    Console.WriteLine("exit");
    Console.WriteLine("ul - get users list");
    Console.WriteLine("cc {user id 1};{user id 2};.. - create chat with members");
    Console.WriteLine("cl - list of chats");
    Console.WriteLine("rc - restore list of chats from server");
    Console.WriteLine("rm {chat id} - restore messages in chat from server");
    Console.WriteLine("vm {chat id} - view messages in chat");
    Console.WriteLine("cm {chat id} {text} - write message");
    Console.WriteLine("em {message id} {text} - edit message");
    Console.WriteLine("dm {message id} - delete message");
    Console.WriteLine("Enter command:");
    Console.ResetColor();
}

namespace ChatHubClient
{
    internal static class Commands
    {
        public const string GetUsers = "ul";
        public const string CreateChat = "cc";
        public const string ChatsList = "cl";
        public const string RestoreChatsList = "rc";
        public const string RestoreMessages = "rm";
        public const string ViewMessages = "vm";
        public const string CreateMessage = "cm";
        public const string EditMessage = "em";
        public const string DeleteMessage = "dm";
    }

    internal static class HubMessages
    {
        public const string MessageSent = nameof(MessageSent);
        public const string MessageEdited = nameof(MessageEdited);
        public const string MessageDeleted = nameof(MessageDeleted);
    }
}