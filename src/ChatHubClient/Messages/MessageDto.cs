﻿namespace ChatHubClient.Messages;

public record MessageDto(Guid Id, Guid ChatId, string Text, DateTime CreatedAt, MemberDto Sender);