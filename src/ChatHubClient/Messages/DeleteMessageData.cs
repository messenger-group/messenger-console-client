﻿namespace ChatHubClient.Messages;

public record DeleteMessageData(Guid MessageId);