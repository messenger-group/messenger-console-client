﻿namespace ChatHubClient.Messages;

public record MessageCreatedData(Guid MessageId, Guid ChatId, string Text, MemberDto Sender);