﻿namespace ChatHubClient.Messages;

public record MemberDto(Guid UserId, string FullName);