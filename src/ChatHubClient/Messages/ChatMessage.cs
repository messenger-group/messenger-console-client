﻿namespace ChatHubClient.Messages;

public class ChatMessage
{
    public required Guid Id { get; init; }
    public required string Text { get; set; }
    public required string Sender { get; init; }

    public override string ToString()
    {
        return $"Id: {Id}; Sender: {Sender}; Text: {Text}";
    }
}