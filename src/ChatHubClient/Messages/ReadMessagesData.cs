﻿namespace ChatHubClient.Messages;

public record ReadMessagesData(Guid ChatId);