﻿namespace ChatHubClient.Messages;

public record MessageEditedData(Guid MessageId, Guid ChatId, string Text);