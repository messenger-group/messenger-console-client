﻿namespace ChatHubClient.Messages;

public record MessageDeletedData(Guid MessageId, Guid ChatId);