﻿namespace ChatHubClient.Messages;

public record ChatDto(Guid Id, List<MemberDto> Members);