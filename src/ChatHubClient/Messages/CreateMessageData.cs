﻿namespace ChatHubClient.Messages;

public record CreateMessageData(Guid ChatId, string Text);