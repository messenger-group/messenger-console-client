﻿namespace ChatHubClient.Messages;

public record ChatData(Dictionary<Guid, ChatMessage> Messages);