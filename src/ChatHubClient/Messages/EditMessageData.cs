﻿namespace ChatHubClient.Messages;

public record EditMessageData(Guid MessageId, string Text);