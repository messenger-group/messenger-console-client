﻿using System.Net.Http.Json;
using ChatHubClient.Messages;

namespace ChatHubClient.Services;

public class UsersClient : IDisposable
{
    private record CreateUserResponse(Guid Id);

    private readonly HttpClient _httpClient;

    public Guid CurrentUserId { get; private set; }

    public UsersClient(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    public void Login(Guid userId)
    {
        CurrentUserId = userId;
    }

    public async Task CreateNewUserAndLogin(string name, string surname, string? patronymic, string email)
    {
        var response = await _httpClient.PostAsJsonAsync("api/users", new
        {
            Name = name,
            Surname = surname,
            Patronymic = patronymic,
            Email = email
        });

        if (response.IsSuccessStatusCode is false)
        {
            throw new Exception("Unable to create user");
        }

        Login((await response.Content.ReadFromJsonAsync<CreateUserResponse>())!.Id);
    }

    public Task<List<UserDto>> GetUsers()
    {
        return _httpClient.GetFromJsonAsync<List<UserDto>>("api/users")!;
    }

    public void Dispose()
    {
        _httpClient.Dispose();
    }
}