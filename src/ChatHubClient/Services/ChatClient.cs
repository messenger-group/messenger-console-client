﻿using System.Net.Http.Json;

namespace ChatHubClient.Services;

public class ChatClient : IDisposable
{
    private readonly HttpClient _httpClient;

    public ChatClient(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    public async Task<Guid> CreateChat(List<Guid> userIds)
    {
        var response = await _httpClient.PostAsJsonAsync("api/chat", new
        {
            Members = userIds
        });

        if (response.IsSuccessStatusCode is false)
        {
            throw new Exception("Unable to create chat");
        }

        return await response.Content.ReadFromJsonAsync<Guid>();
    }

    public void Dispose()
    {
        _httpClient.Dispose();
    }
}