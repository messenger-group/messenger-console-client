﻿namespace ChatHubClient.Services;

public interface IOutputWriter
{
    void WriteLine(string message);
}