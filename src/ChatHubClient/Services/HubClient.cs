﻿namespace ChatHubClient.Services;

using Messages;

public class HubClient
{
    private readonly Dictionary<Guid, ChatData> _chats = [];
    private readonly IOutputWriter _outputWriter;
    private readonly Guid _currentUserId;

    public HubClient(IOutputWriter outputWriter, Guid currentUserId)
    {
        _outputWriter = outputWriter;
        _currentUserId = currentUserId;
    }

    public List<Guid> GetChats()
    {
        return _chats.Keys.ToList();
    }

    public List<ChatMessage> GetMessages(Guid chatId)
    {
        return GetOrCreateChatData(chatId).Messages.Values.ToList();
    }

    public void RestoreMessages(Guid chatId, IEnumerable<ChatMessage> messages)
    {
        var chatData = GetOrCreateChatData(chatId);

        chatData.Messages.Clear();

        foreach (var message in messages)
        {
            chatData.Messages.Add(message.Id, message);
        }
    }

    public void RestoreChats(IEnumerable<Guid> userChats)
    {
        foreach (var userChat in userChats)
        {
            if (_chats.ContainsKey(userChat) is false)
            {
                _chats.TryAdd(userChat, new ChatData([]));
            }
        }
    }

    public void OnMessageCreated(MessageCreatedData data)
    {
        var chatData = GetOrCreateChatData(data.ChatId);

        chatData.Messages.Add(data.MessageId, new ChatMessage
        {
            Id = data.MessageId,
            Text = data.Text,
            Sender = data.Sender.FullName
        });

        if (data.Sender.UserId != _currentUserId)
        {
            _outputWriter.WriteLine($"You have new message in chat {data.ChatId}");
        }
    }

    public void OnMessageEdited(MessageEditedData data)
    {
        var chatData = GetOrCreateChatData(data.ChatId);

        if (chatData.Messages.TryGetValue(data.MessageId, out var message) is false)
        {
            return;
        }

        message.Text = data.Text;

        _outputWriter.WriteLine($"Message in chat {data.ChatId} edited");
    }

    public void OnMessageDeleted(MessageDeletedData data)
    {
        var chatData = GetOrCreateChatData(data.ChatId);

        chatData.Messages.Remove(data.MessageId);

        Console.WriteLine($"Message in chat {data.ChatId} deleted");
    }

    private ChatData GetOrCreateChatData(Guid chatId)
    {
        if (_chats.TryGetValue(chatId, out var chatData))
        {
            return chatData;
        }

        chatData = new ChatData([]);
        _chats.TryAdd(chatId, chatData);

        return chatData;
    }
}